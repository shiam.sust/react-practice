import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Radium from 'radium';
import Person from './Person/person';
import { StyleRoot } from 'radium/lib';

class App extends Component {
  state = {
    persons: [
      { id: 'asdbdakfj', name: 'Shiam', age: 26 },
      { id: 'afdgvegsd', name: 'MMRS', age: 25 },
      { id: 'gsfafeeef', name: 'Shopon', age: 25}
    ],
    
    showPersons : false

  }

  deletePersonsHandler = (personIndex) => {
    //const persons = this.state.persons.slice();
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({
      persons: persons
    })
  }

  nameChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    //const person = this.state.persons[personIndex];

    const person = {
      ...this.state.persons[personIndex]
    };

    //const person = Object.assign({}, this.state.persons[personIndex]);

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({
      persons: persons
    })
  }

  togglePersonHandler = () => {
    
    const doesShow = this.state.showPersons;
    this.setState({
      showPersons : !doesShow,
    });
  }

  render() {
    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      marginRight: '5px',
      ':hover' : {
        backgroundColor: 'lightgreen',
        color: 'black'
      }
    }

    let persons = null;

    if(this.state.showPersons){
      persons = (
        <div>
          {this.state.persons.map((person, index) => {
              return <Person
                click={this.deletePersonsHandler.bind(this, index)}
                name={person.name}
                age={person.age}
                key={person.id}
                changed={(event) => this.nameChangedHandler(event, person.id)} />
            })
          }
        </div>
      );

      style.backgroundColor = 'red';
      style[':hover'] = {
        backgroundColor : 'salmon',
        color:'white'
      };
    }

    let color = [];
    if(this.state.persons.length <= 2){
      color.push('red');
    }
    if(this.state.persons.length <= 1){
      color.push('bold');
    }

    return (
      <StyleRoot>
        <div className="App">
          <h1>Hello, this is my first react app!</h1>
          <h4 className={color.join(' ')}>This is working perfectly!</h4>
          <button 
            style={style}
            onClick={this.togglePersonHandler.bind(this, 0)}>Switch
          </button>
          {persons}
        </div>
      </StyleRoot>
      
    );
  }
}

export default Radium(App);
