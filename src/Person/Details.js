import React from 'react';

import './details.css';

const detail = (props) => {
    return (
        <div className="Details">
            <p>Education: {props.education}</p>
            <p>Contact Number: {props.contact} </p>
            <p>Email: {props.email}</p>
        </div>
    );
};

export default detail;